\documentclass[12pt,a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[francais]{babel}

\usepackage{cmbright}

%\usepackage[thmmarks]{ntheorem}
\usepackage{ntheorem}

\usepackage{fancyhdr}

%\newtheorem{theo}{Théorème}
%\newtheorem{definition}{Définition}
%\newtheorem{propriete}{Propriété}
%\newtheorem{remarque}{Remarque}
%\newtheorem{exemple}{Exemple}

%\pagestyle{empty}

\newenvironment{liste}{%
  \renewcommand{\labelitemi}{$\bullet$}%
          \begin{itemize}}{\end{itemize}}



\title{Solides et volumes}
\date{}
\author{Cinquième}


\begin{document}

\maketitle

\section{Rappels de 6ème}
\label{sec:rappels6}

\subsection{Définition et premières propriétés}
\label{sec:definition}

Les solides sont des objets de l'espace. En classe de 6ème, on se
contente d'étudier la pavé droit ou parallélépipède
rectangle.
\begin{definition}
  Un parallélépipède rectangle est un solide formé de faces
  rectangulaires deux  à deux parallèles.
\end{definition}
\begin{propriete}
  Un parallélépipède rectangle comporte 6 faces, 8 sommets et 12 arêtes.
\end{propriete}

\subsection{Représentations}
\label{sec:representation}

\subsubsection{Patrons}
\label{sec:patrons}

\begin{definition}
  Un patron est l'enchaînement de faces permettant la construction du solide.
\end{definition}
\begin{remarque}
  Un même solide peut comporter plusieurs patrons.
\end{remarque}

\subsubsection{Perspective cavalière}
\label{sec:perspect_caval}

La perspective cavalière est une des façons de représenter un solide
sur une surface plane. Il existe d'autres techniques, comme le point de
fuite. La représentation d'un solide en perspective cavalière doit
répondre aux contraintes suivantes :
\begin{propriete}
  \begin{liste}
  \item les arêtes visibles sont dessinées en traits pleins
  \item les arêtes parallèles sur le solide doivent être parallèle sur
    la représentation
  \item les angles de la face de devant sont conservés
  \item les arêtes cachées sont dessinées en traits pointillés
  \end{liste}
\end{propriete}

\subsection{Mesure de volume}
\label{sec:mesure_de_volume}

\subsubsection{Unités métriques}
\label{sec:unites_metriques}

On utilise comme unités de base le $\mathrm{cm}^3$, le $\mathrm{dm}^3$ et
le $\mathrm{m}^3$ qui correspondent respectivement  à des cubes
d'arête 1~cm, 1~dm et 1~m.

Il faut mille ($1\,000$) cubes d'un centimètre d'arête pour remplir un cube d'un
décimètre d'arête et mille ($1\,000$) d'un décimètre d'arête pour remplir
un cube d'arête un mètre.

\begin{propriete}
  \begin{liste}
    \item $1\ \mathrm{dm}^3=1\,000\ \mathrm{cm}^3$
    \item $1\ \mathrm{m}^3=1\,000\ \mathrm{dm}^3$
  \end{liste}
\end{propriete}

\subsubsection{Unités usuelles}
\label{sec:unite_usuelle}

On utilise dans la vie courante une autre unité : le litre, noté
L.
\begin{definition}
  Un litre est un autre nom pour 1 décimètre cube.

  \begin{center}\fbox{$1\ \mathrm{L} = 1\ \mathrm{dm}^3$}\end{center}
\end{definition}

Cette unité possède les multiples habituels à l'exception de
kilo. Ainsi on a :

\begin{minipage}{0.45\linewidth}
  \begin{liste}
    \item $1\ \mathrm{L}=10\ \mathrm{dL}$
    \item $1\ \mathrm{dL}=10\ \mathrm{cL}$
    \item $1\ \mathrm{cL}=10\ \mathrm{mL}$
  \end{liste}
\end{minipage}
\begin{minipage}{0.45\linewidth}
  \begin{liste}
    \item $1\ \mathrm{daL}=10\ \mathrm{L}$
    \item $1\ \mathrm{hL}=10\ \mathrm{daL}=100\ \mathrm{L}$
    \item $10\ \mathrm{hL}=1\,000\ \mathrm{L}=1\ \mathrm{m}^3$
  \end{liste}
\end{minipage}

\subsubsection{Volume du parallélépipède rectangle}
\label{sec:volume_parallelelpipede}

\begin{propriete}
  Le volume $V$ d'un parallélépipède rectangle de longueur $L$, de largeur
  $l$ et de hauteur $h$ est le produit de la longueur par la largeur
  par la hauteur.

  \begin{center}\fbox{$V=L\times l\times h$}\end{center}
\end{propriete}

\section{Le prisme droit}
\label{sec:prisme_droit}

\subsection{Définition}
\label{sec:definition_prisme}

\begin{definition}
  Un prisme droit est un volume composé de deux faces polygonales
  quelconques parallèles entre elles liées par des faces
  rectangulaires.
\end{definition}

Un prisme droit peut se réaliser en assemblant des parallélépipèdes
rectangles de même hauteur. On peut éventuellement être amenés à
découper certains de ceux-ci en deux (cas du prisme à base triangulaire.)

\subsection{Volume du prisme}
\label{sec:volume_prisme}

\begin{propriete}
  Le volume d'un prisme droit est le produit de l'aire de sa base par
  la hauteur de ce prisme.

  \begin{center}\fbox{$V=A_{\mathrm{base}}\times h$}\end{center}
\end{propriete}

\begin{preuve}
  On découpe le prisme droit en parallélépipède rectangles qu'on
  complète par des demis-parallélépipèdes. Comme la hauteur des
  parallélépipède est commune, on peut factoriser la somme des aires
  de bases par cette hauteur $h$.
\end{preuve}

\begin{remarque}
  Attention à ne pas confondre hauteur de la base (dans le cas du
  triangle et du parallélogramme) et hauteur du prisme.
\end{remarque}

\section{Le cylindre de révolution}
\label{sec:cylindre}

\subsection{Définition}
\label{sec:definition_cylindre}

\begin{definition}
  Un cylindre de révolution est le volume obtenu en faisant tourner un
  rectangle autour d'un axe.
\end{definition}

Le cylindre complète finalement le prisme droit (à base
polygonale) en envisageant le cas où la base est un disque.

Sous ces conditions, la propriété suivante se justifie par un
raisonnement analogue à celui fait pour l'aire du disque.

\begin{propriete}
  Le volume d'un disque de rayon $r$ et de hauteur $h$ est le produit
  de la hauteur, du carré du rayon de $\pi$ et de 2.

  \begin{center}\fbox{$V=2\times \pi\times r^2\ \times h$}\end{center}
\end{propriete}

\end{document}
