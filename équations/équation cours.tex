\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}
\newtheorem{regle}{Règle}
\newtheorem{theoreme}{Théorème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}
\title{\'Equation}
\author{3\up{ème}}
\date{}

\begin{document}

\maketitle\\
\begin{itemize}
\item Résoudre une équation mise sous la forme $A\times B=0$, où $A$ et $B$ désignent deux
	expressions du premier degré de la même variable.
\item Mettre en équation et résoudre un problème conduisant à une  équation	du premier degré. 
\end{itemize}



\section{\'Equations du premier degré}
\begin{theoreme}
 L'équation $ax+b=0$ avec $a\neq0$ a une seule solution :
$\displaystyle{-{b\over a}}$.
\end{theoreme}
\begin{exemple}
 L' équation $4x+5=0$ a une solution : \rep{$-{5\over4}$}.\\
 Vérification : $4\times(-{5\over4})+5=-5+5=0$.\\
\end{exemple}
\begin{preuve}
 Soit l'équation $ax+b=0$.\\
  Par soustraction de chaque côté de l'égalité, on  obtient :
 $ax+b-b=0-b$, c'est à dire : $ax=-b$.\\
  Puis, par division de  chaque côté de l'égalité, on  obtient :
 $ax\div a=-b\div a$, d'où $x=-{b\over a}$.
\end{preuve}

\section{\'Equations du type $(ax+b)(cx+d)=0$}
\begin{regle}
 Si un  produit est nul, alors l'un des termes au moins est nul. C'est à dire :
 si $A\times B=0$ alors $A=0$ ou $B=0$.
\end{regle}
\begin{theoreme}
 L'équation $(ax+b)(cx+d)=0$ a au maximum deux solutions : $-{b\over a}$ et 
 $-{d\over c}$.
\end{theoreme}
\begin{remarque}
 Si  $-{b\over a}=-{d\over c}$ alors il n'y a qu'une seule solution $-{b\over a}=-{d\over c}$
\end{remarque}
\begin{exemple}
 $(x+3)(3x-6)=0$ donc 
 \begin{tabular}[t]{ccc}
  $x+3=0$ & ou &  $3x-6=0$\\
  $x=-3$  & ou & $3x=6$\\
  $x=-3$  & ou & $x=2$\\
 \end{tabular}\\
 L'équation a deux solutions : \rep{$-3$} et \rep{$2$}.
\end{exemple}
\begin{exemple}
 $(2x-3)(4x-6)=0$ donc
 \begin{tabular}[t]{ccc}
  $2x-3=0$ & ou & $4x-6=0$ \\
  $2x=3$   & ou & $4x=6$ \\
  $x={2\over3}$ & ou & $x={6\over4}= {2\over3}$\\
 \end{tabular}\\
 L'équation a une solution : \rep{${2\over3}$}.
\end{exemple}
\begin{exemple}
$x^2-(3x-4)^2=0$. En factorisant, on obtient $(x+(3x-4))(x-(3x-4))=0$ soit
$(4x-4)(-2x+4)=0$.\\
L'équation a deux solutions : ${4\over4}=\rep{1}$ et ${-4\over-2}=\rep{2}$.
\end{exemple}
\section{Mise en équation}
\begin{enumerate}
\item Choisir l'inconnue, il faut repérer la valeur que l'on cherche.
\item Exprimer les informations de l'énoncé en fonction de l'inconnue.
\item Résoudre l'équation.
\item Vérification
\item Conclure
\end{enumerate}

\begin{exemple}
Aujourd'hui, Marc a 11 ans et Pierre a 26 ans.\\
Dans combien d'années l'âge de Pierre sera-t-il le double de celui de Marc?
\end{exemple}
On pose $x$ le nombre d'années.\\
L'âge de Marc dans $x$ années sera : \rep{$11+x$}\\
L'âge de Pierre dans $x$ années sera : \rep{ $26+x$}\\
On a donc : \rep{ $26+x=2\times(11+x)$}\\
Résolution :\\
\rep{$26+x=22+2x$\\
$26-22+x=22+2x-22$\\
$4+x=2x$\\
$4+x-x=2x-x$\\
$4=x$}\\
Vérification : \rep{$11+4=15$\\
$26+4=30$\\
30 est le double de 15.}\\
Réponse : \rep{Dans 4 ans l'âge de Pierre sera le double de celui de Marc}
\end{document}



