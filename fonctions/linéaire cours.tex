\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}


\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{Collège \textsc{La Salle Saint-Denis}}}}
\rfoot{\footnotesize{Page \thepage/ \pageref{LastPage}}}
\rhead{}
\lhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}


\makeatletter
\renewcommand{\@evenfoot}%

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}


\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{\textcolor{teal}{Définition}}
\newtheorem{propriete}{\textcolor{violet}{Propriété}}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textcolor{cyan}{\textsf{\textsc{\small{Démonstration}}}}}
\newtheorem{exemple}{\textcolor{purple}{\textsf{\textsc{\small{Exemple\\}}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{\textcolor{magenta}{Remarque}}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}
\newtheorem{regle}{Règle}
\newtheorem{theoreme}{\textcolor{red}{Théorème}}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}
\title{Fonction linéaire}
\author{3\up{ème}}
\date{}

\begin{document}

\maketitle\\
  \begin{itemize}
  \item Déterminer l'expression algébrique d'une fonction linéaire à partir de la donnée d'un nombre non nul et de son image.
  \item Représenter graphiquement une fonction linéaire.
  \item Connaître et utiliser la relation $y=ax$ entre les coordonnées $(x,y)$ d'un point M qui est caractéristique de son appartenance à la droite représentative de la fonction linéaire $x\mapsto ax$.
    \item Lire et interpréter graphiquement le coefficient d'une fonction linéaire représentée par une droite.
  \end{itemize}
  
  \section{Définition}
\begin{definition}
Une {\em fonction linéaire} est une fonction pouvant être définie par
$f:x\mapsto ax$, où $a$ est un nombre.
\end{definition}
\begin{exemple}
$f:x\mapsto 2x$\qquad$g:x\mapsto-\displaystyle{1\over2}x$
\end{exemple}

\section{Lien avec la proportionnalité}
\begin{propriete}
La courbe représentative d'une fonction linéaire ${f:x\mapsto ax}$ est
une droite passant par l'origine du repère. L'équation de cette droite est
$y=ax$.\\
$a$ est appelé le
{\em coefficient directeur} de la droite.
\end{propriete}
\begin{center}
\begin{tikzpicture}
\tkzInit[xmin=-4,xmax=4,ymin=-3,ymax=3]
\tkzAxeXY
\draw[gray, dashed] (-4,-3)grid(4,3);
\draw[blue, very thick, domain=-1.5:1.5] plot(\x,{2*\x});
\draw[violet, very thick, domain=-4:4] plot(\x,{-0.5*\x});
\end{tikzpicture}
\end{center}

\begin{regle}
Soit $f$, une fonction numérique définie par $f:x\mapsto ax$. \\
$f(x)$ est proportionnel à $x$.\\
 Le coefficient de proportionnalité est $a$.
\end{regle}

Pour tracer cette droite, il faut calculer l'image d'un nombre.
\begin{exemple} On considère la fonction linéaire $f(x)=-0,5x$.\\
On calcule l'image du nombre $2$ par la fonction $f$. 
\[f(2)=-0,5\times 2\]
\[f(2)=-1\]
On place le point de coordonnées $(2;-1)$, de plus la droite passe par l'origine du repère.
\end{exemple}


\section{Déterminer une fonction linéaire}
\begin{regle}
Lorsqu'on connaît un nombre non nul
et son image par une fonction linéaire, cela suffit
pour déterminer la fonction linéaire.
\end{regle}
\begin{exemple}
Soit $f$, une fonction linéaire telle que $f(7)=2$. Déterminer $f$.\\
En notant $a$, le coefficient de proportionnalité, alors pour tout nombre $x$, on a $f(x)=ax$.\\ 
En particulier pour $x=7$,
\[f(7)=a\times7\]
\[a\times7=2\]
\[a=\dfrac{2}{7}\]
Donc $f$ est définie par $f:x\mapsto{2\over7}x$.
\end{exemple}

\begin{center}
\begin{tikzpicture}
\tkzInit[xmin=-6,xmax=6,ymin=-8,ymax=	8]
\tkzAxeXY
\draw[gray, dashed] (-6,-8)grid(6,8);
\draw[blue, very thick, domain=-4:4] plot(\x,{2*\x});
\draw[violet, very thick, domain=-6:6] plot(\x,{-0.5*\x});
\draw [ red, very thick,->] (1,2) node[below right ]{$+1$} -|(2,4) node[right]{$+2$};
\draw [ red, very thick,->] (-4,2)node[above right ]{$+1$} -|(-3,1.5) node[right]{$-0,5$};
\end{tikzpicture}
\end{center}
\end{document}



