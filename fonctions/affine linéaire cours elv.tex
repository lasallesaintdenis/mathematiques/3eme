\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}


\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{Collège \textsc{La Salle Saint-Denis}}}}
\rfoot{\footnotesize{Page \thepage/ \pageref{LastPage}}}
\rhead{}
\lhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}


\makeatletter
\renewcommand{\@evenfoot}%

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}


\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{\textcolor{teal}{Définition}}
\newtheorem{propriete}{\textcolor{violet}{Propriété}}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textcolor{cyan}{\textsf{\textsc{\small{Démonstration}}}}}
\newtheorem{exemple}{\textcolor{purple}{\textsf{\textsc{\small{Exemple\\}}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{\textcolor{magenta}{Remarque}}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}
\newtheorem{regle}{Règle}
\newtheorem{theoreme}{\textcolor{red}{Théorème}}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}
\title{Fonction affine - Fonction linéaire}
\author{3\up{ème}}
\date{}

\begin{document}

\maketitle\\
  
  \section{Fonction linéaire}
  \subsection{Définition}
\begin{definition}
Une \textcolor{red}{fonction linéaire} est une fonction pouvant être définie par
$f:x\mapsto ax$, où $a$ est un nombre.
\end{definition}
\begin{exemple}
$f:x\mapsto 2x$\qquad$g:x\mapsto-\displaystyle{1\over2}x$
\end{exemple}

\subsection{Représenter une fonction linéaire}
\begin{propriete}
La \textcolor{red}{courbe représentative} d'une fonction linéaire ${f:x\mapsto ax}$ est
une droite passant par l'origine du repère. L'équation de cette droite est de la forme $y=ax$.\\
\textcolor{red}{$a$} est appelé le \textcolor{red}{coefficient directeur} de la droite.
\end{propriete}
\begin{center}
\begin{tikzpicture}
\tkzInit[xmin=-6,xmax=6,ymin=-4,ymax=4]
\tkzAxeXY
\draw[gray, dashed] (-6,-4)grid(6,4);
\end{tikzpicture}
\end{center}
\begin{regle}
Soit $f$, une fonction numérique définie par $f:x\mapsto ax$. \\
$f(x)$ est proportionnel à $x$.\\
 Le coefficient de proportionnalité est $a$.\\
 
 Pour tracer une droite, il faut :
 \begin{itemize}
 \item calculer l'image d'un nombre;
 \item placer le point dans le repère;
 \item tracer la droite passant par l'origine du repère et ce point.
\end{itemize}  
\end{regle}
\pagebreak
\begin{exemple} Tracer la droite $y=-0,5x$. On considère la fonction linéaire $f(x)=-0,5x$.
\begin{enumerate}
\item Calculer l'image du nombre $2$ par la fonction $f$. \\
\ligne{3}
\item Placer le point dans le repère.
\item Tracer  la droite.
\end{enumerate}


Tracer la droite $y=2x$.
\begin{enumerate}
\item Calculer l'image du nombre $-1$ par la fonction $g(x)=2x$. \\
\ligne{3}
\item Placer le point dans le repère.
\item Tracer la droite.
\end{enumerate}

\end{exemple}


\subsection{Déterminer une fonction linéaire}
\begin{regle}
Lorsqu'on connaît un nombre non nul
et son image par une fonction linéaire, cela suffit
pour déterminer la fonction linéaire.
\end{regle}
\begin{exemple}
Soit $f$, une fonction linéaire telle que $f(7)=2$. Déterminer $f$.\\
En notant $a$, le coefficient de proportionnalité, alors pour tout nombre $x$, on a $f(x)=ax$.\\ 
En particulier pour $x=7$,\\

on remplace $x$ par 7 dans la fonction $f$ :  \dotfill\\

on sait que $f(7)=2$ donc : \dotfill\\

on calcule $a$ : \dotfill\\

Donc $f$ est définie par : \dotfill
\end{exemple}
\pagebreak
\subsection{Lecture graphique}

\begin{center}
\begin{tikzpicture}
\tkzInit[xmin=-6,xmax=6,ymin=-6,ymax=6]
\tkzAxeXY
\draw[gray, dashed] (-6,-6)grid(6,6);
\draw[blue, very thick, domain=-3:3] plot(\x,{2*\x});
\draw[violet, very thick, domain=-6:6] plot(\x,{-0.5*\x});
\draw [ red, very thick,->] (1,2) node[below right ]{$+1$} -|(2,4) node[right]{$+2$};
\draw [ red, very thick,->] (-4,2)node[above right ]{$+1$} -|(-3,1.5) node[right]{$-0,5$};
\end{tikzpicture}
\end{center}
\section{Fonction affine}
\begin{definition}
Une \textcolor{red}{fonction affine} est une fonction pouvant être définie par
${f:x\mapsto ax+b}$, où $a$ et $b$ sont des  nombres.
\end{definition}

\begin{exemple}
$f:x\mapsto 2x+1$\qquad$g:x\mapsto-\displaystyle{1\over2}x+2$
\end{exemple}

\begin{remarque}  
Une fonction  linéaire est aussi une fonction affine
(cas particulier où $b=0$).
\end{remarque}

\begin{propriete}
La courbe représentative d'une fonction affine définie par ${f:x\mapsto ax+b}$ est une droite.\\
L'équation de cette droite est $y=ax+b$. $a$ est appelé le \textcolor{red}{coefficient directeur} de la droite et $b$ est appelé \textcolor{red}{ l'ordonnée à l'origine} de la droite.
\end{propriete}

Pour tracer une droite, i faut connaître les coordonnées de deux points.\\
 Pour tracer une droite, il faut :
 \begin{itemize}
 \item calculer l'image de deux nombres;
 \item placer les points dans le repère;
 \item tracer la droite passant par ces points.
\end{itemize}  

\begin{exemple} Tracer la droite $y=3x-2$. On considère la fonction linéaire $h(x)=3x-2$.\\
\begin{enumerate}
\item Calculer l'image du nombre $2$ par la fonction $h$. \\
\ligne{3}
\item Placer le point dans le repère.
\item Calculer l'image du nombre $-1$ par la fonction $h$. \\
\ligne{3}
\item Placer le point dans le repère.
\item Tracer la droite $y=3x-2$.
\end{enumerate}

\end{exemple}

\begin{center}
\begin{tikzpicture}
\tkzInit[xmin=-7,xmax=7,ymin=-7,ymax=7]
\tkzAxeXY
\draw[gray, dashed] (-7,-7)grid(7,7);
\end{tikzpicture}
\end{center}


\end{document}



