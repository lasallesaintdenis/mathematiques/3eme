\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}
\newtheorem{notation}{Notation}
\newtheorem{regle}{Règle}
\newtheorem{theoreme}{Théorème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}
\title{Notion de fonction}
\author{3\up{ème}}
\date{}

\begin{document}

\maketitle\\

  \begin{itemize}
  \item Déterminer l'image d'un nombre par une fonction déterminée par une courbe, un tableau de données ou une formule.
    \item Déterminer un antécédent par lecture directe dans un tableau    ou sur une représentation graphique.
    \item Comprendre et s'exprimer en utilisant les langages mathématiques.    
    \item Exprimer une grandeur en fonction d'une autre
    \item Modéliser une situation par une fonction numérique
    \item Utiliser le langage formel des fonctions
    \item Représenter par un graphique.
  \end{itemize}

\section{Notion de fonction}
\begin{definition}
Une \textcolor{red} { fonction numérique} est un procédé qui à un nombre associe un nombre et un \textbf{seul} .
\end{definition}

\begin{definition}
Soit une fonction $f$. Si $f(a)=b$ alors :
\begin{itemize}
\item $b$ est l' \textcolor{red}{image} de $a$ par la fonction $f$. \quad L'image d'un nombre est unique.
\item $a$ est un \textcolor{red}{antécédent} de $b$ par la fonction $f$. \quad Un nombre $b$ peut avoir plusieurs antécédents.
\end{itemize}
\end{definition}


\begin{exemple}
On définit la fonction $f$ comme étant le procédé qui élève un nombre au carré, multiplie le résultat par $0,5$ et enlève $2$. \\
1 devient alors \rep{$0,5\times 1^2-2=-1,5$} \\

5 devient \rep{$0,5\times 5^2-2=10,5$}\\

On dit aussi <<\textbf{ l'image} de 1 est \rep{$-1,5$ }>> \\
 <<\textbf{ l'image} de 5 est \rep{$10,5$} >>
\end{exemple}

\begin{notation}

$f$ est le procédé qui élève un nombre au carré, multiplie le résultat par $0,5$ et enlève $2$. \\
$f:x \longmapsto 0,5x^2-2$ \\ $f(x)=0,5x^2-2$ \\ 

Par le procédé $f$, 1 devient $-1,5$ et 5 devient $10,5$ \\
$f:1\longmapsto -1,5$ et  $f:5\longmapsto 10,5$\\
$f(1)=-1,5$ et $f(5)=10,5$ 

\end{notation}

\section{Notion de courbe}
\begin{definition}
\`A une fonction $f$, on associe une courbe dans un repère orthonormé. 
On construit cette courbe point par point, chaque point étant obtenu en choisissant une abscisse $x$ et en prenant comme ordonnée $f(x)$. 
On dit que l'équation de  la  courbe  est $y=f(x)$.
\end{definition}

\begin{exemple}
Soit $f$, la fonction définie par $f:x\mapsto0,5x^2-2$. Voici un tableau de valeurs :
\begin{center}\rep{
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline 
$x$    & $-3$ & $-2$ & $-1$ & 0 & 1& 2 & 3\\ \hline
$f(x)$ & $2,5$ & $0$  & $-1,5$  & $-2$ & $-1,5$ & 0 & $2,5$ \\ \hline
$\left(x;f(x)\right)$ & $(-3;2,5)$ & $(-2;0)$ & $(-1;-1,5)$ &
$(0;-2)$ & $(1;-1,5)$ & $(2;0)$ & $(3;2,5)$\\ \hline
\end{tabular}}
\end{center}
Puis on place les points pour obtenir la {\em courbe représentative} de la fonction $f$ :
\begin{center}
\begin{tikzpicture}
\tkzInit[xmin=-4,xmax=4,ymin=-3,ymax=3]
\tkzAxeXY
\draw[gray , dashed, step=0.5] (-4,-3)grid(4,3);
\draw[blue, very thick, domain=-3:3] plot(\x,{0.5*(\x)*(\x)-2});
\draw[red] (-3,2.5)node{$\bullet$} (-2,0)node{$\bullet$} (-1,-1.5)node{$\bullet$}(0,-2)node{$\bullet$}(1,-1.5)node{$\bullet$}(2,0)node{$\bullet$}(3,2.5)node{$\bullet$};
\end{tikzpicture}
\end{center}
\end{exemple}
\end{document}



